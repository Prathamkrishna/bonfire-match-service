package com.bonfire.bonfirematchservice.dtos.controllerdtos;

import lombok.Data;

@Data
public class UsernameDto {
    private String username;
}
