package com.bonfire.bonfirematchservice.dtos;

import lombok.Data;

@Data
public class UserInfoDto {
    private String useremail;
    private String userprofilelink;
    private String display_name;
}
