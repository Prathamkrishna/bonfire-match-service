package com.bonfire.bonfirematchservice.dtos;

import lombok.Data;

@Data
public class FetchArtistsBasedOnUserFromRepositoryDto {
    private String artist;
    private String artistlink;
    private String artistimage;
}
