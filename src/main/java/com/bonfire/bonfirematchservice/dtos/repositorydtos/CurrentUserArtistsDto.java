package com.bonfire.bonfirematchservice.dtos.repositorydtos;

import lombok.Data;

import java.util.List;

@Data
public class CurrentUserArtistsDto {
    String artist;
}
