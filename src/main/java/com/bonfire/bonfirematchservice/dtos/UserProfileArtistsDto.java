package com.bonfire.bonfirematchservice.dtos;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class UserProfileArtistsDto {
    private String username;
    private List<FetchArtistsBasedOnUserFromRepositoryDto> artists;
    private List<String> genres;
    private String userprofilelink;
}
