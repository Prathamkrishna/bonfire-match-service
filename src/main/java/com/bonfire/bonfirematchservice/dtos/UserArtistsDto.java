package com.bonfire.bonfirematchservice.dtos;

import lombok.Data;

@Data
public class UserArtistsDto {
    private String username;
    private String userprofilelink;
}
