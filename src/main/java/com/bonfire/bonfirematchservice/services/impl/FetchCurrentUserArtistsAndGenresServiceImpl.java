package com.bonfire.bonfirematchservice.services.impl;

import com.bonfire.bonfirematchservice.dtos.repositorydtos.CurrentUserArtistsDto;
import com.bonfire.bonfirematchservice.repositories.FetchCurrentUserArtistsAndGenresRepository;
import com.bonfire.bonfirematchservice.services.interfaces.FetchCurrentUserArtistsAndGenresService;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

@Service
public class FetchCurrentUserArtistsAndGenresServiceImpl implements FetchCurrentUserArtistsAndGenresService {

    private final FetchCurrentUserArtistsAndGenresRepository fetchCurrentUserArtistsAndGenresRepository;

    public FetchCurrentUserArtistsAndGenresServiceImpl(FetchCurrentUserArtistsAndGenresRepository fetchCurrentUserArtistsAndGenresRepository) {
        this.fetchCurrentUserArtistsAndGenresRepository = fetchCurrentUserArtistsAndGenresRepository;
    }


    @Override
    public Flux<CurrentUserArtistsDto> getCurrentUserArtists(String username) {
        return fetchCurrentUserArtistsAndGenresRepository.getCurrentUserArtistsDtoByCurrentUser(username);
    }
}
