package com.bonfire.bonfirematchservice.services.impl;

import com.bonfire.bonfirematchservice.dtos.UserArtistsDto;
import com.bonfire.bonfirematchservice.dtos.UserProfileArtistsDto;
import com.bonfire.bonfirematchservice.dtos.controllerdtos.UsernameDto;
import com.bonfire.bonfirematchservice.dtos.repositorydtos.CurrentUserArtistsDto;
import com.bonfire.bonfirematchservice.repositories.FetchSimilarProfilesBasedOnProfiles;
import com.bonfire.bonfirematchservice.services.interfaces.FetchCurrentUserArtistsAndGenresService;
import com.bonfire.bonfirematchservice.services.interfaces.FetchUserProfilesService;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

@Service
public class FetchUserProfilesServiceImpl implements FetchUserProfilesService {

    private final FetchSimilarProfilesBasedOnProfiles fetchSimilarProfilesBasedOnProfiles;
    private final FetchCurrentUserArtistsAndGenresService currentUserArtistsAndGenresService;

    public FetchUserProfilesServiceImpl(FetchSimilarProfilesBasedOnProfiles fetchSimilarProfilesBasedOnProfiles, FetchCurrentUserArtistsAndGenresService currentUserArtistsAndGenresService) {
        this.fetchSimilarProfilesBasedOnProfiles = fetchSimilarProfilesBasedOnProfiles;
        this.currentUserArtistsAndGenresService = currentUserArtistsAndGenresService;
    }

    @Override
    public Mono<ServerResponse> fetchProfiles(ServerRequest serverRequest) {
        /* TODO for next iteration
        *   Create a match for genre and artist at the same time */

        var usernameDtoMono = serverRequest.bodyToMono(UsernameDto.class).flatMap(val->{
            return Mono.just(val.getUsername());
        });
        var x = usernameDtoMono.
                flatMapMany(currentUserArtistsAndGenresService::getCurrentUserArtists)
                .collectList();
        var y = fetchListOfUser(x, usernameDtoMono);

//        var mapFlux = fetchSimilarProfilesBasedOnProfiles.getRelatedProfilesByArtists(
//                "Travis Scott", "G-Eazy", "Doja Cat", "blackbear", "no", "prathamkrishna"
//        );
//        var xyz = Flux.concat(mapFlux.collectList(), mapFlux.collectList().flatMap(val->{
//            if (val.size() < 10){
////                fetch by genres
//
//                return fetchSimilarProfilesBasedOnProfiles.getRelatedProfilesByArtists(
//                        "Travis Scott", "G-Eazy", "Doja Cat", "blackbear", "no", "prathamkrishna"
//                ).collectList();
//            }
//            return Mono.empty();
//        })).flatMap(val-> Mono.just(val).flatMapMany(Flux::fromIterable));
//        y.subscribe(System.out::println);
        return ServerResponse.ok()
                .contentType(MediaType.TEXT_EVENT_STREAM)
                .body(y, UserArtistsDto.class);
//                .body(fetchCompleteUserDetails(xyz).delayElements(Duration.ofSeconds(1)), UserProfileArtistsDto.class);
    }

    protected Flux<UserArtistsDto> fetchListOfUser(Mono<List<CurrentUserArtistsDto>> currentUser, Mono<String> stringMono){
//        change to mono, with DS map and then access keys

        var c = currentUser.flatMapMany(val->{
            System.out.println(val + " v");

//            return stringMono.flatMapMany(x->{
//                System.out.println(x + " x");
            stringMono.flux().doOnNext(v->{
                System.out.println(v + " v");
            }).subscribe();
                return fetchSimilarProfilesBasedOnProfiles.getRelatedProfilesByArtists(
                    val.get(0).getArtist(), val.get(1).getArtist(), val.get(2).getArtist(), val.get(3).getArtist(), val.get(4).getArtist(), "prathamkrishna");
//            });
        });
        return c;
//        return Flux.empty();
    }

    protected Flux<UserProfileArtistsDto> fetchCompleteUserDetails(Flux<UserArtistsDto> listFlux){
        return listFlux.flatMap(item-> fetchSimilarProfilesBasedOnProfiles.getUserArtists(item.getUsername()).collectList().flatMap(x->{
            UserProfileArtistsDto userProfileArtistsDto = new UserProfileArtistsDto();
            userProfileArtistsDto.setUsername(item.getUsername());
            userProfileArtistsDto.setUserprofilelink(item.getUserprofilelink());
            userProfileArtistsDto.setArtists(x);
            return Mono.just(userProfileArtistsDto);
        })).flatMap(item-> fetchSimilarProfilesBasedOnProfiles.getUserGenres(item.getUsername()).collectList().flatMap(val->{
            item.setGenres(val);
            return Mono.just(item);
        }));
    }
}
