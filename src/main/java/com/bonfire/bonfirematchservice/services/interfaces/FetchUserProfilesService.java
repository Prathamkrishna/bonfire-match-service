package com.bonfire.bonfirematchservice.services.interfaces;

import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface FetchUserProfilesService {
    Mono<ServerResponse> fetchProfiles(ServerRequest serverRequest);
}
