package com.bonfire.bonfirematchservice.services.interfaces;

import com.bonfire.bonfirematchservice.dtos.repositorydtos.CurrentUserArtistsDto;
import reactor.core.publisher.Flux;

public interface FetchCurrentUserArtistsAndGenresService {
    Flux<CurrentUserArtistsDto> getCurrentUserArtists(String username);
}
