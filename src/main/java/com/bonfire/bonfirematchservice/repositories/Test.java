package com.bonfire.bonfirematchservice.repositories;

import com.bonfire.bonfirematchservice.dtos.UserArtistsDto;
import com.bonfire.bonfirematchservice.dtos.UserInfoDto;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface Test extends ReactiveCrudRepository<UserInfoDto, Integer> {
    @Query("SELECT userartists.username, userartists.artist, userartists.artistlink, userartists.artistimage, userinfo.userprofilelink, usergenres.genre from userartists JOIN userinfo ON userartists.username = userinfo.display_name JOIN usergenres ON userartists.username = usergenres.username where (userartists.username <> \"prathamkrishna\") AND (artist = \"Travis Scott\" OR artist = \"G-eazy\" OR artist = \"Hole\") LIMIT 10;\n")
    Flux<UserArtistsDto> getInfo();


}
