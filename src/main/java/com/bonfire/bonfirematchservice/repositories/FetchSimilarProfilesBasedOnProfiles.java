package com.bonfire.bonfirematchservice.repositories;

import com.bonfire.bonfirematchservice.dtos.FetchArtistsBasedOnUserFromRepositoryDto;
import com.bonfire.bonfirematchservice.dtos.UserArtistsDto;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface FetchSimilarProfilesBasedOnProfiles extends ReactiveCrudRepository<UserArtistsDto, Integer> {
    @Query("SELECT userartists.username, userinfo.userprofilelink from userartists JOIN userinfo ON userartists.username = userinfo.display_name where (userartists.username <> :currentUser) AND (artist = :artist1 OR artist = :artist2 OR artist = :artist3 OR artist = :artist4 OR artist = :artist5) GROUP BY userartists.username LIMIT 10;")
    Flux<UserArtistsDto> getRelatedProfilesByArtists(@Param("artist1") String artist1, @Param("artist2") String artist2, @Param("artist3") String artist3, @Param("artist4") String artist4, @Param("artist5") String artist5, @Param("currentUser") String currentUser);

    @Query("SELECT usergenres.username, userinfo.userprofilelink from usergenres JOIN userinfo ON usergenres.username = userinfo.display_name where (usergenres.username <> :currentUser) AND (genre = :genre1 OR genre = :genre2 OR genre = :genre3 OR genre = :genre4 OR genre = :genre5) GROUP BY usergenres.username LIMIT :remainingCount;")
    Flux<UserArtistsDto> getRelatedProfilesByGenres(@Param("genre1") String genre1, @Param("genre2") String genre2, @Param("genre3") String genre3, @Param("genre4") String genre4, @Param("genre5") String genre5, @Param("currentUser") String currentUser, @Param("remainingCount") int remainingCount);

    @Query("SELECT userartists.username, userartists.artist, userartists.artistlink, userartists.artistimage from userartists where username = :username;")
    Flux<FetchArtistsBasedOnUserFromRepositoryDto> getUserArtists(@Param("username") String username);

    @Query("SELECT usergenres.genre from usergenres where username = :username;")
    Flux<String> getUserGenres(@Param("username") String username);

}
