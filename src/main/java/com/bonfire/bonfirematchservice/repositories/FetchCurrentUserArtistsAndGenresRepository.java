package com.bonfire.bonfirematchservice.repositories;

import com.bonfire.bonfirematchservice.dtos.repositorydtos.CurrentUserArtistsDto;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface FetchCurrentUserArtistsAndGenresRepository extends ReactiveCrudRepository<CurrentUserArtistsDto, Integer> {
    @Query("SELECT userartists.artist from userartists where userartists.username = :currentUser;")
    Flux<CurrentUserArtistsDto> getCurrentUserArtistsDtoByCurrentUser(@Param("currentUser") String currentUser);
}
