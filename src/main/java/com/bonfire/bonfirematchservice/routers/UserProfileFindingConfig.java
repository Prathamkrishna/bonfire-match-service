package com.bonfire.bonfirematchservice.routers;

import com.bonfire.bonfirematchservice.services.interfaces.FetchUserProfilesService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;

@Configuration
public class UserProfileFindingConfig {

    private final FetchUserProfilesService fetchUserProfilesService;

    public UserProfileFindingConfig(FetchUserProfilesService fetchUserProfilesService) {
        this.fetchUserProfilesService = fetchUserProfilesService;
    }

    @Bean
    public RouterFunction<ServerResponse> routerFunction(){
        return RouterFunctions.route()
            .POST("/here", fetchUserProfilesService::fetchProfiles)
                .build();
    }
}
